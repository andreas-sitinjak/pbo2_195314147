package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import pbo2.Mahasiswa;
import pbo2.penduduk;

/**
 *
 * @author ASUS
 */
public class MahasiswaTable extends JDialog implements ActionListener {

    private JTable jt;
    private JScrollPane sp;
    private Mahasiswa mhs;
    private JLabel label_title;
    private JLabel label_nim;
    private JLabel label_nama;
    private JLabel label_ttl;
    private JTextField text_nim;
    private JTextField text_nama;
    private JTextField text_ttl;
    private JTextArea area_nama;

    public MahasiswaTable() {
        init();
    }

    public void init() {
        this.setLayout(null);

        label_title = new JLabel("List Mahasiswa");
        label_title.setBounds(110, 10, 200, 20);
        this.add(label_title);

        label_nama = new JLabel("Nama\t :");
        label_nama.setBounds(80, 60, 100, 50);
        this.add(label_nama);
        String asu = "";
        System.out.println(MainFrame.anggota.length);
        for (int i = 0; i < MainFrame.anggota.length; i++) {
            if (MainFrame.anggota[i] != null) {
                asu = asu + " " + MainFrame.anggota[i].getNama();
            }

        }
        area_nama = new JTextArea(asu);
        area_nama.setBounds(180, 75, 180, 20);
        this.add(area_nama);

        label_nim = new JLabel("NIM \t :");
        label_nim.setBounds(80, 100, 100, 50);
        this.add(label_nim);
        text_nim = new JTextField(MainFrame.anggota[0].getNama());
        text_nim.setBounds(180, 115, 180, 20);
        this.add(text_nim);
        label_ttl = new JLabel("Tempat & tanggal lahir\t :");
        label_ttl.setBounds(80, 140, 100, 50);
        this.add(label_ttl);
        text_ttl = new JTextField(MainFrame.anggota[0].getTempatTanggalLahir());
        text_ttl.setBounds(180, 155, 180, 20);
        this.add(text_ttl);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

}
