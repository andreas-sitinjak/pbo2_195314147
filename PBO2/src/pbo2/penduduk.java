package pbo2;

public abstract class penduduk {

    private String Nama;
    private String TempatTanggalLahir;

    public penduduk() {

    }

    public penduduk(String dataNama,String dataTempatTanggalLahir) {
        this.Nama = dataNama;
        this.TempatTanggalLahir=dataTempatTanggalLahir;
    }

    public void setNama(String Nama) {
        this.Nama = Nama;
    }

    public String getNama() {
        return Nama;
    }

    public void setTempatTanggalLahir(String TempatTanggalLahir) {
        this.TempatTanggalLahir = TempatTanggalLahir;
    }

    public String getTempatTanggalLahir() {
        return TempatTanggalLahir;
    }
    abstract double hitungIuran();

}
