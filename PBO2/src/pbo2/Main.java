package pbo2;

public class Main {

    public static void main(String[] args) {
        UKM ukm = new UKM("SEPAK BOLA");
        Mahasiswa ketua = new Mahasiswa();
        Mahasiswa sekretaris =new Mahasiswa();
        ketua.setNama("ANDRE");
        ketua.setnim("195314147");
        sekretaris.setNama("NIKO");
        sekretaris.setnim("195314145");
        
        ukm.setketua(ketua);
        ukm.setsekretaris(sekretaris);
        
        System.out.println("NAMA KETUA      : "+ukm.getketua().getNama());
        System.out.println("NIM  KETUA      : "+ukm.getketua().getnim());
        System.out.println("NAMA SEKRETARIS : "+ukm.getsekretaris().getNama());
        System.out.println("NIM  SEKRETARIS : "+ukm.getsekretaris().getnim());
        
        Mahasiswa S = new Mahasiswa("19531434", "Mario", "Stabat, 17 april 2001");
        ukm.setsekretaris(S);
        penduduk[] pen = new penduduk[3];
        Mahasiswa[] A = new Mahasiswa[2];
        A[0] = new Mahasiswa();
        A[0].setNama("Dika");
        A[0].setnim("195314201");
        A[0].setTempatTanggalLahir("Aceh, 1 juni 1980");
        pen[0] = A[0];
        A[1] = new Mahasiswa();
        A[1].setNama("Mika");
        A[1].setnim("195314133");
        A[1].setTempatTanggalLahir("Semarang, 2 Desember 1989");
        pen[1] = A[1];
        Masyarakat[] mas = new Masyarakat[1];
        mas[0] = new Masyarakat();
        mas[0].setNama("Melati");
        mas[0].setnomor("721");
        mas[0].setTempatTanggalLahir("Sulawesi, 5 Maret 1988");
        pen[2] = mas[0];

        System.out.println("");
        System.out.println("===============" + ukm.getnamaUnit() + "===============");
        System.out.println("");
        double tot = 0;
        double iuranMaha = 0, iuranMasya = 0;
        for (int i = 0; i < pen.length; i++) {
            System.out.println("ANGGOTA KE " + (i + 1) + "");
            System.out.println("nama : " + pen[i].getNama());
            System.out.println("Tempat Tanggal Lahir : " + pen[i].getTempatTanggalLahir());
            if (pen[i] instanceof Mahasiswa) {
                Mahasiswa mahas = (Mahasiswa) pen[i];
                System.out.println("Nim : " + mahas.getnim());
                System.out.println("Iuran : " + mahas.hitungIuran());
                iuranMaha = +mahas.hitungIuran();
            } else if (pen[i] instanceof Masyarakat) {
                Masyarakat masy = (Masyarakat) pen[i];
                System.out.println("Nomor : " + masy.getnomor());
                System.out.println("Iuran : " + masy.hitungIuran());
                iuranMasya = +masy.hitungIuran();
            }
            System.out.println("");
            System.out.println("============================================");
        }
        tot = iuranMaha + iuranMasya;
        System.out.println("Total Iuran : " + tot);
    }

}
