package pbo2;

public class Mahasiswa extends penduduk {

   private String nim;

    public Mahasiswa() {
    }

    public Mahasiswa(String nim, String dataNama, String tempatTgllahir) {
        this.nim = nim;
    }

    public void setnim(String nim) {
        this.nim = nim;
    }

    public String getnim() {
        return nim;
    }

    @Override
    public double hitungIuran() {
        double value = Double.parseDouble(nim);
        return value /10000;
 
    }
}
