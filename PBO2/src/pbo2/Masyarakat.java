package pbo2;

public class Masyarakat extends penduduk {

    private String nomor;

    public Masyarakat() {
    }

    public Masyarakat(String nomor, String tempatTgllahir, String dataNama) {
        this.nomor = nomor;
    }

    public void setnomor(String nomor) {
        this.nomor = nomor;
    }

    public String getnomor() {
        return nomor;
    }

    @Override
    double hitungIuran() {
        double value = Double.parseDouble(nomor);
        return value * 100;
        
    }

}
